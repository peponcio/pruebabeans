package formBeans;

import beans.PersonaBean;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.primefaces.context.RequestContext;

@ManagedBean
@SessionScoped
public class PersonaFormBean implements Serializable{
    @ManagedProperty(value="#{personaBean}")
    private PersonaBean personaBena;
    
    public void verSecundario(){
        RequestContext.getCurrentInstance().execute("PF('segundo').show();");
    }
    
    public void verTercero(){
        RequestContext.getCurrentInstance().execute("PF('tercero').show();");
    }
    
    public void cerrarTercero(){
        RequestContext.getCurrentInstance().execute("PF('tercero').hide();PF('segundo').hide();");
    }
    
    public PersonaFormBean() {
    }
    
    public PersonaBean getPersonaBena() {
        return personaBena;
    }

    public void setPersonaBena(PersonaBean personaBena) {
        this.personaBena = personaBena;
    }
    
    
}
