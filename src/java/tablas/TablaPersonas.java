/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablas;

import dominio.Persona;
import java.util.List;

/**
 *
 * @author 4rK
 */
public class TablaPersonas {
    private static List<Persona> tablaPersonas;

    /**
     * @return the tablaPersonas
     */
    public static List<Persona> getTablaPersonas() {
        return tablaPersonas;
    }

    /**
     * @param aTablaPersonas the tablaPersonas to set
     */
    public static void setTablaPersonas(List<Persona> aTablaPersonas) {
        tablaPersonas = aTablaPersonas;
    }

    public TablaPersonas() {
    }
    
    
    
}
